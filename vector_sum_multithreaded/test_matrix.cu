
//-*- c++ -*-                                                                                                                                    \
\                                                                                                                                                 
                                                                                                                                                 \

#include <iostream>
#include <cuda.h>
#include <cuda_runtime.h>

#define BLOCK_SIZE 256

using namespace std;



__global__ void sum_matrix(int* matrix, int num_rows, int num_cols, int* result) {
    __shared__ int partial_sum[256];

    int tid = threadIdx.x;
    int row_idx = tid / num_cols;
    int col_idx = tid % num_cols;

    int row_offset = row_idx * num_cols;

    int sum = 0;
    for (int i = col_idx; i < num_cols; i += 256) {
        for (int j = row_idx; j < num_rows; j += 256) {
            sum += matrix[row_offset + i + j * num_cols];
        }
    }

    partial_sum[tid] = sum;

    // Wait for all threads in the block to finish computing their partial sums
    __syncthreads();

    // Sum the partial sums computed by each thread in the block
    for (int i = 128; i > 0; i /=2) {
        if (tid < i) {
            partial_sum[tid] += partial_sum[tid + i];
        }
        __syncthreads();
    }

    // Write the final sum to global memory
    if (tid == 0) {
        *result = partial_sum[0];
    }
}

void sum_matrix_on_gpu(int* matrix, int num_rows, int num_cols, int* result) {
    int* d_matrix;
    int* d_result;
    cudaMalloc(&d_matrix, num_rows * num_cols * sizeof(int));
    cudaMalloc(&d_result, sizeof(int));
    cudaMemcpy(d_matrix, matrix, num_rows * num_cols * sizeof(int), cudaMemcpyHostToDevice);
    sum_matrix<<<1, 256>>>(d_matrix, num_rows, num_cols, d_result);
    cudaMemcpy(result, d_result, sizeof(int), cudaMemcpyDeviceToHost);
    cudaFree(d_matrix);
    cudaFree(d_result);
}

int main() {
    int num_rows = 10;
    int num_cols = 10;

    // Allocate memory for the input matrix and fill it with random values
    int* matrix = new int[num_rows * num_cols];
    for (int i = 0; i < num_rows * num_cols; i++) {
        matrix[i] = 1.0;
    }

    // Allocate memory for the output result
    int* result = new int[1];

    // Compute the sum of the matrix on the GPU
    sum_matrix_on_gpu(matrix, num_rows, num_cols, result);

    // Print the result
    printf("Sum of the matrix: %d\n", *result);


    // Verify the result
    int expected_result = 0;
    for (int i = 0; i < num_rows * num_cols; i++) {
      expected_result += matrix[i];
    }
    if (*result == expected_result) {
      printf("Sum of the matrix: %d\n", *result);
    } else {
      printf("ERROR: Expected %d but got %d\n", expected_result, *result);
    }



    // Free memory
    delete[] matrix;
    delete[] result;

    return 0;
}
