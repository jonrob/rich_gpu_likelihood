
//-*- c++ -*-                                                                                                                                    \
\                                                                                                                                                 
                                                                                                                                                 \

#include <iostream>
#include <cuda.h>
#include <cuda_runtime.h>

#define BLOCK_SIZE 256

using namespace std;



__global__ void sumMatrix(int* d_matrix, int* d_result, int size)
{
  __shared__ int s_array[BLOCK_SIZE];
  int tid = threadIdx.x;
  int i = blockIdx.x * blockDim.x + threadIdx.x;
  s_array[tid] = (i < size) ? d_matrix[i] : 0;
  __syncthreads();

  for (unsigned int s = blockDim.x / 2; s > 0; s >>= 1)
    {
      if (tid < s)
        {
	  s_array[tid] += s_array[tid + s];
        }
      __syncthreads();
    }

  if (tid == 0)
    {
      atomicAdd(d_result, s_array[0]);
    }
}

int main()
{
  int size = 1024 * 1024; // 1M elements
  int* h_matrix = (int*)malloc(size * sizeof(int));
  int* d_matrix;
  cudaMalloc(&d_matrix, size * sizeof(int));
  for (int i = 0; i < size; ++i)
    {
      h_matrix[i] = 1.0;
    }
  cudaMemcpy(d_matrix, h_matrix, size * sizeof(int), cudaMemcpyHostToDevice);

  int* d_result;
  cudaMalloc(&d_result, sizeof(int));
  cudaMemset(d_result, 0, sizeof(int));

  dim3 block(BLOCK_SIZE, 1, 1);
  dim3 grid((size + block.x - 1) / block.x, 1, 1);

  cout << grid.x << endl;

  sumMatrix<<<1, block>>>(d_matrix, d_result, size);

  int h_result;
  cudaMemcpy(&h_result, d_result, sizeof(int), cudaMemcpyDeviceToHost);

  printf("Sum of matrix: %d\n", h_result);

  free(h_matrix);
  cudaFree(d_matrix);
  cudaFree(d_result);

  return 0;
}
