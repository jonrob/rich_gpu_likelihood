
//-*- c++ -*-                                                                                                                                    \
\                                                                                                                                                 
\                                                                                                                                                \

                                                                                                                                                 \
\

#include <iostream>
#include <cuda.h>
#include <cuda_runtime.h>

#define BLOCK_SIZE 256

using namespace std;



__global__ void matrix_sum(float* d_matrix, int* d_dims, float* d_result) {
    int tid = threadIdx.x;
    int dim_x = d_dims[0];
    int dim_y = d_dims[1];
    int total_size = dim_x * dim_y;

    float thread_sum = 0;
    for (int i = tid; i < total_size; i += blockDim.x) {
        int row = i / dim_x;
        int col = i % dim_x;
        if (row < dim_y && col < dim_x) {
            thread_sum += d_matrix[row * dim_x + col];

	    //if(col==dim_x-1){

	    //thread_sum=thread_sum*3.0;

	    //}


        }
    }




    __shared__ float s_partial_sums[256];
    s_partial_sums[tid] = thread_sum;
    __syncthreads();

    for (int i = 128; i > 0; i /= 2) {
        if (tid < i) {
            s_partial_sums[tid] += s_partial_sums[tid + i];
        }
        __syncthreads();
    }

    if (tid == 0) {
        d_result[0] = s_partial_sums[0];
    }
}

void matrix_sum_wrapper(float* h_matrix, int* h_dims, float* h_result) {
    int size_x = h_dims[0];
    int size_y = h_dims[1];
    int total_size = size_x * size_y;

    float* d_matrix;
    int* d_dims;
    float* d_result;

    cudaMalloc((void**)&d_matrix, total_size * sizeof(float));
    cudaMalloc((void**)&d_dims, 2 * sizeof(int));
    cudaMalloc((void**)&d_result, sizeof(float));

    cudaMemcpy(d_matrix, h_matrix, total_size * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_dims, h_dims, 2 * sizeof(int), cudaMemcpyHostToDevice);

    matrix_sum<<<1, 256>>>(d_matrix, d_dims, d_result);

    cudaMemcpy(h_result, d_result, sizeof(float), cudaMemcpyDeviceToHost);

    cudaFree(d_matrix);
    cudaFree(d_dims);
    cudaFree(d_result);
}

int main() {
  float h_matrix[14] = {1, 2, 3, 4, 5, 6, 9, 10, 1, 3, 56, 577, 234, 23};
    int h_dims[2] = {7, 2};
    float h_result[1];

    float sum=0;

    for(int i=0;i<14;i++){

      sum=sum+h_matrix[i];


    }

    cout << sum << endl;



    matrix_sum_wrapper(h_matrix, h_dims, h_result);

    printf("The sum of the matrix is %f\n", h_result[0]);

    return 0;
}
