//-*- c++ -*-                                                                                                                                     

#include <stdio.h>
#include <iostream>
#include <stdlib.h>
#include <time.h>
#include <ctime>

#include "utils.h"
#include "timer.h"
#include "scan.h"



__global__ void sumVector(float* a, float* result, int n) {
  int idx = blockIdx.x * blockDim.x + threadIdx.x;
  int stride = blockDim.x * gridDim.x;
  float sum = 0.0f;

  for (int i = idx; i < n; i += stride) {
    sum += a[i];
  }

  atomicAdd(result, sum);



}

int main() {
  int n = 1024;
  float* a, * result;
  float* dev_a, * dev_result;

  // Allocate memory on host
  a = (float*)malloc(n * sizeof(float));
  result = (float*)malloc(sizeof(float));

  // Initialize vector with some values
  for (int i = 0; i < n; ++i) {
    a[i] = i;
  }

  // Allocate memory on device
  cudaMalloc((void**)&dev_a, n * sizeof(float));
  cudaMalloc((void**)&dev_result, sizeof(float));

  // Copy vector from host to device
  cudaMemcpy(dev_a, a, n * sizeof(float), cudaMemcpyHostToDevice);
  cudaMemcpy(dev_result, result, sizeof(float), cudaMemcpyHostToDevice);

  // Calculate block size and number of blocks
  int blockSize = 256;
  int numBlocks = (n + blockSize - 1) / blockSize;

  // Call kernel function
  //sumVector <<< numBlocks, blockSize >>> (dev_a, dev_result, n);

  sumVector <<< 1, blockSize >>> (dev_a, dev_result, n);

  // Copy result from device to host
  cudaMemcpy(result, dev_result, sizeof(float), cudaMemcpyDeviceToHost);

  // Print result
  printf("Sum of vector is: %f\n", *result);

  // Free memory on host and device
  free(a);
  free(result);
  cudaFree(dev_a);
  cudaFree(dev_result);

  return 0;
}
