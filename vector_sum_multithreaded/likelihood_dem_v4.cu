//-*- c++ -*- 						\
\                                                                                                                                               
\                                                                                                                                                

                                                                                                                         

#include <iostream>
#include <cuda.h>
#include <cuda_runtime.h>
#include <fstream>
#include <string>
#include <sstream>

#include <map>
#include <vector>



#define BLOCK_SIZE 256

using namespace std;


int n_pixel=13000;

int n_hyp=4;

int n_tracks=128;

int nphotons=20; //max number of photons associated to 1 pixel     

int max_pixel_per_track=1000;


__device__ int d_n_pixel=13000;

__device__ int d_n_hyp=4;

__device__ int d_n_tracks=128;

__device__ int d_nphotons=20;

__device__ int d_max_pixel_per_track=1000;


__device__ int pid_types[4]={0,1,2,3};




void FillArrayWithMCDataPhotonContainer(float *&photon_sig_container, float *&photon_sig_track){



  std::vector<std::vector<int>> track_pion(n_tracks);
  std::vector<std::vector<int>> track_muon(n_tracks);
  std::vector<std::vector<int>> track_kaon(n_tracks);
  std::vector<std::vector<int>> track_proton(n_tracks);
  

  std::vector<std::vector<float>> sig_pion(n_tracks);
  std::vector<std::vector<float>> sig_muon(n_tracks);
  std::vector<std::vector<float>> sig_kaon(n_tracks);
  std::vector<std::vector<float>> sig_proton(n_tracks);
  

  
  int index;

  for(int i=0;i<n_tracks;i++){

    for(int j=0;j<max_pixel_per_track;j++){


      for(int z=0;z<n_hyp;z++){



	index= i*max_pixel_per_track*n_hyp+j*n_hyp+z;

	photon_sig_container[index]=0;


	photon_sig_track[index]=-1;


      }




    }


  }

  





  
  std::string pixel, signal, hyp, tk_index, phot_index, phot_id;




  std::string line;
  std::ifstream myfile ("likelihood_params_newformat.txt");
  if (myfile.is_open())
    {
      while ( getline (myfile,line) )
        {


	  std::stringstream ss(line);

          ss >> pixel >> signal >> hyp >> tk_index >> phot_index >> phot_id;

	  if(hyp=="pion" && std::stoi(tk_index)< n_tracks){


	    track_pion[std::stoi(tk_index)].push_back(std::stoi(pixel));

	    sig_pion[std::stoi(tk_index)].push_back(std::stod(signal));


	  }



	  
	  if(hyp=="muon" && std::stoi(tk_index)< n_tracks){


            track_muon[std::stoi(tk_index)].push_back(std::stoi(pixel));

	    sig_muon[std::stoi(tk_index)].push_back(std::stod(signal));


          }




	  if(hyp=="kaon" && std::stoi(tk_index)< n_tracks){


            track_kaon[std::stoi(tk_index)].push_back(std::stoi(pixel));

	    sig_kaon[std::stoi(tk_index)].push_back(std::stod(signal));


          }



	  if(hyp=="proton" && std::stoi(tk_index)< n_tracks){


            track_proton[std::stoi(tk_index)].push_back(std::stoi(pixel));

	    sig_proton[std::stoi(tk_index)].push_back(std::stod(signal));


          }





	}




    }



  

  // fill vector


  for(int i=0;i<n_tracks;i++){

    for(int j=0;j<max_pixel_per_track;j++){


      for(int z=0;z<n_hyp;z++){



        index= i*max_pixel_per_track*n_hyp+j*n_hyp+z;



	if(z==0){ //muon

	  if(j<sig_muon[i].size()){
	  

	  photon_sig_container[index]=sig_muon[i].at(j);


	  photon_sig_track[index]=track_muon[i].at(j);

	  }



	}





	if(z==1){ //pion


	  if(j<sig_pion[i].size()){


	    photon_sig_container[index]=sig_pion[i].at(j);


	    photon_sig_track[index]=track_pion[i].at(j);

          }

	  



	}

	

	if(z==2){ //kaon


	  if(j<sig_kaon[i].size()){


	    photon_sig_container[index]=sig_kaon[i].at(j);


	    photon_sig_track[index]=track_kaon[i].at(j);

          }




	}



	
	if(z==3){ //proton


	  if(j<sig_proton[i].size()){


	    photon_sig_container[index]=sig_proton[i].at(j);


	    photon_sig_track[index]=track_proton[i].at(j);

          }





	}








      }




    }


  }






  //Print to check

  /*  
  for(int i=0;i<n_tracks;i++){


    cout << " track:" <<i <<" ";


    for(int j=0;j<max_pixel_per_track;j++){


      for(int z=0;z<n_hyp;z++){



	index= i*max_pixel_per_track*n_hyp+j*n_hyp+z;

	
	if(z==1){


	  cout << photon_sig_track[index] << " ";




	}



      }




    }

    cout << endl;





    }*/










  /*   for(int i=0;i<n_tracks;i++){


    cout << "track " << i <<": size " << sig_pion[i].size() <<endl;


     for(int j=0;j<(int)sig_pion[i].size();j++){


      cout << sig_pion[i].at(j) << " ";




      }


      cout << endl;




      }*/









}










void FillArrayWithMCDataTrack(float *&track_sig){


  int index;


  for(int i=0;i<n_tracks;i++){


    for(int j=0;j<n_hyp;j++){


      index=i*n_hyp+j;

      track_sig[index]=0;


    }




  }



  std::map<std::string, int> pid_mapping;


  pid_mapping.insert(std::make_pair("muon", 0));
  pid_mapping.insert(std::make_pair("pion", 1));
  pid_mapping.insert(std::make_pair("kaon", 2));
  pid_mapping.insert(std::make_pair("proton",3));




  // read track part for the likelihood                                                                                             
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////        

  std::string track, electron, muon, pion, kaon, proton, deuteron, below_threshold;


  std::string line2;


  std::ifstream myfile2 ("likelihood_track_part.txt");
  if (myfile2.is_open())
    {
      while ( getline (myfile2,line2) )
	{


	  std::stringstream ss(line2);

	  ss >> track >> electron >> muon >> pion >> kaon >> proton >> deuteron >> below_threshold;


	  //index=std::stoi(track)*n_hyp+pid_mapping[hyp];

	  if(std::stoi(track)<n_tracks){



	    //index=std::stoi(track);                                                                                               
	    
	    track_sig[std::stoi(track)*n_hyp+pid_mapping["pion"]]=std::stod(pion);

	    //cout << std::stod(pion) << " " << pid_mapping["pion"] << endl;

	    track_sig[std::stoi(track)*n_hyp+pid_mapping["proton"]]=std::stod(proton);

	    track_sig[std::stoi(track)*n_hyp+pid_mapping["kaon"]]=std::stod(kaon);

	    track_sig[std::stoi(track)*n_hyp+pid_mapping["muon"]]=std::stod(muon);



	  }




	}




    }







}







void FillArrayWithMCDataPixel(float * &pixel_sig, int * &pixel_tk, int * &pixel_ph, int *&pid_vec, int *&new_pid, float *&p_sig, float *&log_p_sig, float *&ddl_container){


  //i = (k1 * d2 * d3 * d4 * d5) + (k2 * d3 * d4 * d5) + (k3 * d4 * d5) + (k4 * d5) + k5 5D

  //i = (k1 * d2 * d3 * d4) + (k2 * d3 * d4) + (k3 * d4) + k4 4D  

  //i = (k1 * d2 * d3) + (k2 * d3) + k3 3D


  std::map<std::string, int> pid_mapping;


  pid_mapping.insert(std::make_pair("muon", 0));
  pid_mapping.insert(std::make_pair("pion", 1));
  pid_mapping.insert(std::make_pair("kaon", 2));
  pid_mapping.insert(std::make_pair("proton",3));

  //cout << pid_mapping["electron"] << endl;                                                                                                      


  std::cout << pid_mapping["muon"] << std::endl;



  int index;

  

  
  for(int i=0;i<n_tracks;i++){


    for(int j=0;j<n_hyp;j++){


      index=i*n_hyp+j;

      ddl_container[index]=0;



    }
    


  }







  for(int i=0;i<n_tracks;i++){


    pid_vec[i]=1; //set all to pions initially    
    


  }



  for(int i=0;i<n_tracks;i++){


    new_pid[i]=2; //set all to kaons                                                                                                    



  }




  


  for(int i=0;i<n_pixel;i++){


    p_sig[i]=0;

    log_p_sig[i]=0;


  }











  for(int i=0;i<n_pixel;i++){


    for(int j=0;j<nphotons;j++){


      index=i*nphotons+j;

      pixel_ph[index]=-1;

      pixel_tk[index]=-1;


    }




  }







  
  for(int i=0;i<n_pixel;i++){

    for(int j=0;j<nphotons;j++){


	for(int z=0;z<n_hyp;z++){


	  
	  index= i*nphotons*n_hyp+j*n_hyp+z;

	  pixel_sig[index]=0;
  
          


	}




    }


  }







 

  float count=0;



  std::string pixel, signal, hyp, tk_index, phot_index, phot_id;




  std::string line;
  std::ifstream myfile ("likelihood_params_newformat.txt");
  if (myfile.is_open())
    {
      while ( getline (myfile,line) )
	{


	  std::stringstream ss(line);

	  ss >> pixel >> signal >> hyp >> tk_index >> phot_index >> phot_id;

	  //cout << pixel <<" " <<  signal <<" " <<  hyp <<" " <<  tk_index <<" " << phot_index <<" " <<  phot_id << endl;


	  if(std::stoi(tk_index) < n_tracks && std::stoi(phot_id) < nphotons && std::stoi(pixel) < n_pixel){

	  
	  pixel_ph[std::stoi(pixel)*nphotons+std::stoi(phot_id)]=std::stoi(phot_index);

	  pixel_tk[std::stoi(pixel)*nphotons+std::stoi(phot_id)]=std::stoi(tk_index);
	    

	  }


	  //index= i*nphotons*n_tracks*n_hyp+j*n_tracks*n_hyp+z*n_hyp+m;

	  if(std::stoi(pixel) < n_pixel && std::stoi(phot_id) < nphotons && pid_mapping[hyp] < n_hyp){

	  
	    // index=std::stoi(pixel)*nphotons*n_tracks*n_hyp+std::stoi(phot_id)*n_tracks*n_hyp+std::stoi(tk_index)*n_hyp+pid_mapping[hyp];                    
	    index=std::stoi(pixel)*nphotons*n_hyp+std::stoi(phot_id)*n_hyp+pid_mapping[hyp];


	    pixel_sig[index]=std::stod(signal);


	    if(pid_mapping[hyp]==1){

	      count=count+std::stod(signal);

	    }

	    //pixel_sig[index]=-1.0;

	  //cout << pixel_sig[index] << endl;
	  

	  }



	} // loop over file



    }



  cout << count << endl;

 



}




int linear_index_4D(int k1, int k2, int k3, int k4) {

  // return (k1 * d2 * d3 * d4) + (k2 * d3 * d4) + (k3 * d4) + k4;

  if(k1 < n_pixel && k2 < nphotons && k3 < n_tracks && k4 < n_hyp){

  return  k1*nphotons*n_tracks*n_hyp+k2*n_tracks*n_hyp+k3*n_hyp+k4;


  }

  
  else{ return -1;}




}



int linear_index_2D(int k1, int k2) {

  // return (k1 * d2 * d3) + (k2 * d3) + k3;                                                                                

  if(k1 < n_pixel && k2 < nphotons){

    return  k1*nphotons+k2;


  }


  else{ return -2;}




}




int linear_index_3D(int k1, int k2, int k3) {

  // return (k1 * d2 * d3) + (k2 * d3) + k3;                                                                                                      

  if(k1 < n_pixel && k2 < nphotons && k3 < n_hyp){

    return  k1*nphotons*n_hyp+k2*n_hyp+k3;


  }


  else{ return -3;}




}






__device__ int Get_Track_Id(int k1, int k2) {

  // return (k1 * d2 * d3) + (k2 * d3) + k3;                                                                                                      

  if(k1 < d_n_pixel && k2 < d_nphotons){

    return  k1*d_nphotons+k2;


  }


  else{ return -2;}




}











__device__ int GetTrackCurrPID(int track_index, int *track_type_vec)
{
  return track_type_vec[track_index];
}









__device__ void logLikelihood(float* d_pixel, float* d_result, int *d_hyp, int *d_tk, float *d_tk_sig, float *d_p_sig, float *d_log_p_sig) {
    int tid = threadIdx.x;


    float thread_sum = 0;

    float thread_sum_tk=0;

    float global_sum=0;

    int index;

    int tk_id;

    int curr_pid;


    //pixel part


    for (int i = tid; i < d_n_pixel; i += blockDim.x) {

      for(int j=0;j<d_nphotons;j++){
	

	tk_id=Get_Track_Id(i,j);

	
	if(tk_id>0){

	  if(d_tk[tk_id]>=0){

	
	  curr_pid=GetTrackCurrPID(d_tk[tk_id],d_hyp);  


	index= i*d_nphotons*d_n_hyp+j*d_n_hyp+curr_pid;


	
	if(index<d_n_pixel*d_nphotons*d_n_hyp){

	thread_sum += d_pixel[index];

	}


	}




	} // check if track index > 0
	



      } // loop over y axis (photons)



      //calculate here the function

      d_p_sig[i]=thread_sum;


      if(thread_sum>=0.001){

	thread_sum=(double)log(exp((double)thread_sum)-1);


      }


      else{

	thread_sum=(double)log(exp((double)0.001)-1);


      }


      d_log_p_sig[i]=thread_sum;



      global_sum=global_sum+thread_sum;

      thread_sum=0;
     


    } // loop over x axis (hit_pixels)




    __shared__ float s_partial_sums[256];


    s_partial_sums[tid] = global_sum;
    __syncthreads();

    for (int i = 128; i > 0; i /= 2) {
        if (tid < i) {
            s_partial_sums[tid] += s_partial_sums[tid + i];
        }
        __syncthreads();
    }

    if (tid == 0) {
        d_result[0] = s_partial_sums[0];
    }








    //track part
    
    __syncthreads();



    for (int i = tid; i < d_n_tracks; i += blockDim.x) {

      

      index=i*d_n_hyp+GetTrackCurrPID(i,d_hyp);

      if(index<d_n_tracks*d_n_hyp){

        thread_sum_tk += d_tk_sig[index];

      }


    } // loop over tracks


    __shared__ float s_partial_sums_tk[256];


    s_partial_sums_tk[tid] = thread_sum_tk;
    __syncthreads();



    for (int i = 128; i > 0; i /= 2) {
      if (tid < i) {
	s_partial_sums_tk[tid] += s_partial_sums_tk[tid + i];
      }
      __syncthreads();
    }

    if (tid == 0) {
      d_result[0] = s_partial_sums_tk[0]-d_result[0];
    }




    // --------------------------------------------------------------------                                                                         
    // Compute complete likelihood for event with starting hypotheses                                                                               
    // --------------------------------------------------------------------  











}







__device__ void deltaLogLikelihood(float* d_result, int curr_hypo, int new_hypo,int d_tk, float *d_tk_sig, float *d_p_sig, float *d_log_p_sig, float *d_photon_sig_container, float *d_photon_sig_track,float *d_dll_container){

  int tid = threadIdx.x;

  //i track

  // j pixel

  // z hyp
  
  __shared__ float s_partial_sums[256];

  float thread_sum = 0;

  float temp_ll=d_result[0];


  int index_dll;

  int index_new;

  int index_old;

  int index_new_track;

  int index_old_track;

  int tk=d_tk;

  int new_hyp=new_hypo; //kaon

  int old_hyp=curr_hypo; //pion

  int pixel;

  float pixel_contrib_new;

  float pixel_contrib_old;


  float track_contrib_new;

  float track_contrib_old;


  float test_sig;


  //index= i*max_pixel_per_track*n_hyp+j*n_hyp+z;  


  // loop over the photons for this track

  for(int j=0;j<d_n_tracks;j++){

    thread_sum=0;

    tk=j;


  for (int i = tid; i < d_max_pixel_per_track; i += blockDim.x) {

    index_new=tk*d_max_pixel_per_track*d_n_hyp+i*d_n_hyp+new_hyp;
    
    index_old=tk*d_max_pixel_per_track*d_n_hyp+i*d_n_hyp+old_hyp;


    pixel=d_photon_sig_track[index_new];
    


    if(pixel>=0){

      pixel_contrib_new=d_photon_sig_container[index_new];

      pixel_contrib_old=d_photon_sig_container[index_old];


      test_sig=d_p_sig[pixel]+ pixel_contrib_new - pixel_contrib_old; 


      if(test_sig>=0.001){

        test_sig=(double)log(exp((double)test_sig)-1);


      }


      else{

        test_sig=(double)log(exp((double)0.001)-1);


      }


      thread_sum=thread_sum+(d_log_p_sig[pixel]-test_sig);




    } // if pixel id >=0




  


  } // loop over photons associated to given track



  //__shared__ float s_partial_sums[256];


  s_partial_sums[tid] = thread_sum;

  __syncthreads();

  for (int i = 128; i > 0; i /= 2) {
    if (tid < i) {
      s_partial_sums[tid] += s_partial_sums[tid + i];
    }
    __syncthreads();
  }

  if (tid == 0) {
    temp_ll = temp_ll+ s_partial_sums[0];
  }



  //track part                                                                                                                                  

  __syncthreads();


  if(tid==0){

    index_dll=tk*d_n_hyp+new_hyp;


    index_new_track=tk*d_n_hyp+new_hyp;

    index_old_track=tk*d_n_hyp+old_hyp; 


    track_contrib_new=d_tk_sig[index_new_track];

    track_contrib_old=d_tk_sig[index_old_track];

    temp_ll = temp_ll+(track_contrib_new-track_contrib_old);

    d_dll_container[index_dll]=d_result[0]-temp_ll;
    
    //d_result[0]=temp_ll;


  }

  __syncthreads();


  }



}









__device__ void deltaLogLikelihood_v2(float* d_result, int curr_hypo, int new_hypo,int d_tk, float *d_tk_sig, float *d_p_sig, float *d_log_p_sig, float *d_photon_sig_container, float *d_photon_sig_track,float *d_dll_container){



  //int tid = threadIdx.x;

  //i track                                                                                                                                       

  // j pixel                                                                                                                                      

  // z hyp                                                                                                                                        

  float thread_sum = 0;

  float temp_ll=d_result[0];


  int index_dll;

  int index_new;

  int index_old;

  int index_new_track;

  int index_old_track;

  int tk=d_tk;

  int new_hyp=new_hypo; //kaon                                                                                                                    

  int old_hyp=curr_hypo; //pion  


  int pixel;

  float pixel_contrib_new;

  float pixel_contrib_old;


  float track_contrib_new;

  float track_contrib_old;


  float test_sig;



  for (int i = 0; i < d_max_pixel_per_track; i++) {

    index_new=tk*d_max_pixel_per_track*d_n_hyp+i*d_n_hyp+new_hyp;

    index_old=tk*d_max_pixel_per_track*d_n_hyp+i*d_n_hyp+old_hyp;


    pixel=d_photon_sig_track[index_new];


    if(pixel>=0){

      pixel_contrib_new=d_photon_sig_container[index_new];

      pixel_contrib_old=d_photon_sig_container[index_old];


      test_sig=d_p_sig[pixel]+ pixel_contrib_new - pixel_contrib_old;


      if(test_sig>=0.001){

        test_sig=(double)log(exp((double)test_sig)-1);


      }


      else{

        test_sig=(double)log(exp((double)0.001)-1);


      }


      thread_sum=thread_sum+(d_log_p_sig[pixel]-test_sig);




    } // if pixel id >=0                                                                                                                          



  } // loop over photons associated to given track          


  temp_ll = temp_ll+thread_sum;


  index_dll=tk*d_n_hyp+new_hyp;


  index_new_track=tk*d_n_hyp+new_hyp;

  index_old_track=tk*d_n_hyp+old_hyp;


  track_contrib_new=d_tk_sig[index_new_track];

  track_contrib_old=d_tk_sig[index_old_track];

  temp_ll = temp_ll+(track_contrib_new-track_contrib_old);

  d_dll_container[index_dll]=d_result[0]-temp_ll;





}


















__device__ void initBestLogLikelihood(float* d_result, int curr_hypo, int new_hypo,int d_tk, float *d_tk_sig, float *d_p_sig, float *d_log_p_sig,float *d_photon_sig_container, float *d_photon_sig_track,float *d_dll_container){

  int tid = threadIdx.x;


  for (int i = tid; i < d_n_tracks; i += blockDim.x) {


    for(int j=0;j<d_n_hyp;j++){


      deltaLogLikelihood_v2(d_result,1,j,i,d_tk_sig, d_p_sig, d_log_p_sig, d_photon_sig_container, d_photon_sig_track,d_dll_container);



    }





    }



}







__device__ void initBestLogLikelihood_v2(float* d_result, int curr_hypo, int new_hypo,int d_tk, float *d_tk_sig, float *d_p_sig, float *d_log_p_sig,float *d_photon_sig_container, float *d_photon_sig_track,float *d_dll_container){

  int tid = threadIdx.x;


  //for (int i = tid; i < d_n_tracks; i += blockDim.x) {


  //for(int j=0;j<d_n_hyp;j++){


      deltaLogLikelihood(d_result,1,2,90,d_tk_sig, d_p_sig, d_log_p_sig, d_photon_sig_container, d_photon_sig_track,d_dll_container);



      //}



      //}




}













__global__ void SIMDLikelihoodMinimiser(float* d_pixel, float* d_result, int *d_curr_hyp, int *d_new_hyp,int *d_tk, float *d_tk_sig, float *d_p_sig, float *d_log_p_sig, float *d_photon_sig_container, float *d_photon_sig_track,float *d_dll_container){

  int tid = threadIdx.x;
 
  // calculate initial likelihood starting with all particles being pions hypothesis

  logLikelihood(d_pixel, d_result, d_curr_hyp, d_tk, d_tk_sig, d_p_sig, d_log_p_sig);


  initBestLogLikelihood(d_result,1,2,90,d_tk_sig, d_p_sig, d_log_p_sig, d_photon_sig_container, d_photon_sig_track,d_dll_container);


  


}








void matrix_sum_wrapper(float* h_pixel, int* h_dims, float* h_result, int dimensions, int *h_hyp, int* h_tk, int * h_ph, float *h_track_sig, int *h_new_hyp, float *h_p_sig, float *h_log_p_sig,float *h_photon_sig_container, float *h_photon_sig_track, float *h_dll_container) {


  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);




    int size_x = h_dims[0];  // hit pixels
    int size_y = h_dims[1];  // photons
    int size_z = h_dims[2]; // PID types
    int size_m = h_dims[3]; // ntracks


    int total_size = size_x * size_y*size_z;

    float* d_pixel;
    int* d_dims;
    float* d_result;
    int *d_hyp;
    int *d_new_hyp;
    int * d_tk;
    int *d_ph;
    float *d_track_sig;
    float *d_p_sig;
    float *d_log_p_sig;
    float *d_photon_sig_container; 
    float *d_photon_sig_track;
    float *d_dll_container;




    cudaMalloc((void**)&d_pixel, total_size * sizeof(float)); // pixel container
    cudaMalloc((void**)&d_dims, dimensions * sizeof(int));
    cudaMalloc((void**)&d_result, sizeof(float));
    cudaMalloc((void**)&d_hyp, n_tracks*sizeof(int)); // vector of PID hyp
    cudaMalloc((void**)&d_new_hyp, n_tracks*sizeof(int)); // vector of PID hyp
    cudaMalloc((void**)&d_tk, size_x*size_y*sizeof(int)); // track container
    cudaMalloc((void**)&d_ph, size_x*size_y*sizeof(int)); // photons container
    cudaMalloc((void**)&d_track_sig, size_z*size_m*sizeof(float)); // track signal container  
    cudaMalloc((void**)&d_p_sig, n_pixel*sizeof(float)); // track signal container 
    cudaMalloc((void**)&d_log_p_sig, n_pixel*sizeof(float)); // track signal container 
    cudaMalloc((void**)&d_photon_sig_container, n_tracks*n_hyp*max_pixel_per_track*sizeof(float)); // track signal container 
    cudaMalloc((void**)&d_photon_sig_track, n_tracks*n_hyp*max_pixel_per_track*sizeof(float)); // track signal container 
    cudaMalloc((void**)&d_dll_container, n_tracks*n_hyp*sizeof(float));




    cudaMemcpy(d_pixel, h_pixel, total_size * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_dims, h_dims, dimensions * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_hyp, h_hyp, n_tracks * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_new_hyp, h_new_hyp, n_tracks * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_tk, h_tk, size_x*size_y * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_ph, h_ph, size_x*size_y * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_track_sig, h_track_sig, size_z*size_m * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_p_sig, h_track_sig, n_pixel * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_log_p_sig, h_track_sig, n_pixel * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_photon_sig_container, h_photon_sig_container, n_tracks*n_hyp*max_pixel_per_track* sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_photon_sig_track, h_photon_sig_track, n_tracks*n_hyp*max_pixel_per_track * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_dll_container, h_dll_container, n_tracks*n_hyp * sizeof(float), cudaMemcpyHostToDevice);


    cudaEventRecord(start);

    SIMDLikelihoodMinimiser<<<1, 256>>>(d_pixel, d_result, d_hyp,d_new_hyp, d_tk, d_track_sig, d_p_sig, d_log_p_sig,d_photon_sig_container,d_photon_sig_track,d_dll_container);

    cudaEventRecord(stop);

    cudaEventSynchronize(stop);


    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);

    cout << "ms " << milliseconds << endl;


    cudaMemcpy(h_result, d_result, sizeof(float), cudaMemcpyDeviceToHost);


    cudaMemcpy(h_dll_container, d_dll_container, n_tracks*n_hyp * sizeof(float), cudaMemcpyDeviceToHost);






    cudaFree(d_pixel);
    cudaFree(d_dims);
    cudaFree(d_result);
    cudaFree(d_hyp);
    cudaFree(d_new_hyp);
    cudaFree(d_tk);
    cudaFree(d_ph);
    cudaFree(d_track_sig);
    cudaFree(d_p_sig);
    cudaFree(d_log_p_sig);
    cudaFree(d_photon_sig_container);
    cudaFree(d_photon_sig_track);
    cudaFree(d_dll_container);

}

int main() {




  /*
  int n_pixel=13000; // max number of hit pixels

  int n_hyp=4; // track hyp

  int n_tracks=128;// ntracks

  int nphotons=5; //max number of photons associated to 1 pixel  */  




  
  int h_dims[4] = {n_pixel,nphotons,n_hyp,n_tracks};
  

  //int size_evt= nphotons*n_tracks*n_pixel*n_hyp;


  int index;



  float *pixel_container=new float[n_pixel*n_hyp*nphotons]; //contains the number of photons associated to each pixel for each PID hypothesis

  float *photon_sig_container=new float[n_tracks*n_hyp*max_pixel_per_track]; // contains the pixels associated to each track for each PID hypothesis

  float *photon_sig_track=new float[n_tracks*n_hyp*max_pixel_per_track];



  int *tk_container=new int[n_pixel*nphotons]; // contains the tk IDs associated to each pixel

  int *ph_container=new int[n_pixel*nphotons]; // contains the photon ID associated to each pixel

  int *initial_pid=new int[n_tracks]; // initial PID hypothesis

  int *new_pid=new int[n_tracks]; // new PID hypothesis
  
  float *track_sig_container=new float[n_tracks*n_hyp]; // contains the contribution from each track for each PID hypothesis


  float *pixel_sig=new float[n_pixel]; // amount of signal contained in each pixel

  float *log_pixel_sig=new float[n_pixel]; // log(exp(sig)-1) of the amount of signal contained in each pixel



  float *dll_container=new float[n_tracks*n_hyp];
  




  // initialize container


  int PIXEL=140;

  int PHOTON_ID=0;

  int PID=0;


  FillArrayWithMCDataPhotonContainer(photon_sig_container,photon_sig_track);



  FillArrayWithMCDataPixel(pixel_container,tk_container,ph_container,initial_pid,new_pid,pixel_sig,log_pixel_sig, dll_container);

  FillArrayWithMCDataTrack(track_sig_container);

  cout << "tk sig " << track_sig_container[0] << endl;

  /*
  if(linear_index_3D(PIXEL,PHOTON_ID,PID)!=-3){


    cout << " pixel signal is " << pixel_container[linear_index_3D(PIXEL,PHOTON_ID,PID)] <<endl;

    cout << "photon id is " << ph_container[linear_index_2D(PIXEL,PHOTON_ID)] <<endl;

    cout << "track id is " << tk_container[linear_index_2D(PIXEL,PHOTON_ID)] <<endl;
    

    

  }


  else{cout << "index out of range" << endl;}*/


  // cout << h_container[0]<<endl;
  
  //pixel, photon, track, pid

  //  cout << linear_index_4D(141,0,50,0) << endl;


  //cout << h_container[linear_index_4D(142,0,93,1)] <<endl;


  float h_result[1];

   

  matrix_sum_wrapper(pixel_container, h_dims, h_result, 4,initial_pid, tk_container, ph_container,track_sig_container, new_pid, pixel_sig, log_pixel_sig,photon_sig_container,photon_sig_track,dll_container);

    printf("The sum of the matrix is %f\n", h_result[0]);

  
    


    for(int i=0;i<n_tracks;i++){


      for(int j=0;j<n_hyp;j++){


	index=i*n_hyp+j;

	cout << " track "<<i <<", hyp "<< j << " " << dll_container[index] << endl;



      }



    }











    return 0;

  


}
