
//-*- c++ -*-                                                                                                                                    \
\                                                                                                                                               
\                                                                                                                                                

                                                                                                                         

#include <iostream>
#include <cuda.h>
#include <cuda_runtime.h>
#include <fstream>
#include <string>
#include <sstream>

#include <map>
#include <vector>



#define BLOCK_SIZE 256

using namespace std;


int n_pixel=13000;

int n_hyp=4;

int n_tracks=128;

int nphotons=20; //max number of photons associated to 1 pixel     


__device__ int d_n_pixel=13000;

__device__ int d_n_hyp=4;

__device__ int d_n_tracks=128;

__device__ int d_nphotons=20;





void FillArrayWithMCDataTrack(float *&track_sig){


  int index;


  for(int i=0;i<n_tracks;i++){


    for(int j=0;j<n_hyp;j++){


      index=i*n_hyp+j;

      track_sig[index]=0;


    }




  }



  std::map<std::string, int> pid_mapping;


  pid_mapping.insert(std::make_pair("muon", 0));
  pid_mapping.insert(std::make_pair("pion", 1));
  pid_mapping.insert(std::make_pair("kaon", 2));
  pid_mapping.insert(std::make_pair("proton",3));




  // read track part for the likelihood                                                                                             
  //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////        

  std::string track, electron, muon, pion, kaon, proton, deuteron, below_threshold;


  std::string line2;


  std::ifstream myfile2 ("likelihood_track_part.txt");
  if (myfile2.is_open())
    {
      while ( getline (myfile2,line2) )
	{


	  std::stringstream ss(line2);

	  ss >> track >> electron >> muon >> pion >> kaon >> proton >> deuteron >> below_threshold;


	  //index=std::stoi(track)*n_hyp+pid_mapping[hyp];

	  if(std::stoi(track)<n_tracks){



	    //index=std::stoi(track);                                                                                               
	    
	    track_sig[std::stoi(track)*n_hyp+pid_mapping["pion"]]=std::stod(pion);

	    //cout << std::stod(pion) << " " << pid_mapping["pion"] << endl;

	    track_sig[std::stoi(track)*n_hyp+pid_mapping["proton"]]=std::stod(proton);

	    track_sig[std::stoi(track)*n_hyp+pid_mapping["kaon"]]=std::stod(kaon);

	    track_sig[std::stoi(track)*n_hyp+pid_mapping["muon"]]=std::stod(muon);



	  }




	}




    }







}







void FillArrayWithMCDataPixel(float * &pixel_sig, int * &pixel_tk, int * &pixel_ph, int *&pid_vec){


  //i = (k1 * d2 * d3 * d4 * d5) + (k2 * d3 * d4 * d5) + (k3 * d4 * d5) + (k4 * d5) + k5 5D

  //i = (k1 * d2 * d3 * d4) + (k2 * d3 * d4) + (k3 * d4) + k4 4D  

  //i = (k1 * d2 * d3) + (k2 * d3) + k3 3D


  std::map<std::string, int> pid_mapping;


  pid_mapping.insert(std::make_pair("muon", 0));
  pid_mapping.insert(std::make_pair("pion", 1));
  pid_mapping.insert(std::make_pair("kaon", 2));
  pid_mapping.insert(std::make_pair("proton",3));

  //cout << pid_mapping["electron"] << endl;                                                                                                      


  std::cout << pid_mapping["muon"] << std::endl;



  int index;

  
  for(int i=0;i<n_tracks;i++){


    pid_vec[i]=1; //set all to pions initially    
    


  }





  for(int i=0;i<n_pixel;i++){


    for(int j=0;j<nphotons;j++){


      index=i*nphotons+j;

      pixel_ph[index]=-1;

      pixel_tk[index]=-1;


    }




  }







  
  for(int i=0;i<n_pixel;i++){

    for(int j=0;j<nphotons;j++){


	for(int z=0;z<n_hyp;z++){


	  
	  index= i*nphotons*n_hyp+j*n_hyp+z;

	  pixel_sig[index]=0;
  
          


	}




    }


  }



 

  float count=0;



  std::string pixel, signal, hyp, tk_index, phot_index, phot_id;




  std::string line;
  std::ifstream myfile ("likelihood_params_newformat.txt");
  if (myfile.is_open())
    {
      while ( getline (myfile,line) )
	{


	  std::stringstream ss(line);

	  ss >> pixel >> signal >> hyp >> tk_index >> phot_index >> phot_id;

	  //cout << pixel <<" " <<  signal <<" " <<  hyp <<" " <<  tk_index <<" " << phot_index <<" " <<  phot_id << endl;


	  if(std::stoi(tk_index) < n_tracks && std::stoi(phot_id) < nphotons && std::stoi(pixel) < n_pixel){

	  
	  pixel_ph[std::stoi(pixel)*nphotons+std::stoi(phot_id)]=std::stoi(phot_index);

	  pixel_tk[std::stoi(pixel)*nphotons+std::stoi(phot_id)]=std::stoi(tk_index);
	    

	  }


	  //index= i*nphotons*n_tracks*n_hyp+j*n_tracks*n_hyp+z*n_hyp+m;

	  if(std::stoi(pixel) < n_pixel && std::stoi(phot_id) < nphotons && pid_mapping[hyp] < n_hyp){

	  
	    // index=std::stoi(pixel)*nphotons*n_tracks*n_hyp+std::stoi(phot_id)*n_tracks*n_hyp+std::stoi(tk_index)*n_hyp+pid_mapping[hyp];                    
	    index=std::stoi(pixel)*nphotons*n_hyp+std::stoi(phot_id)*n_hyp+pid_mapping[hyp];


	    pixel_sig[index]=std::stod(signal);


	    if(pid_mapping[hyp]==1){

	      count=count+std::stod(signal);

	    }

	    //pixel_sig[index]=-1.0;

	  //cout << pixel_sig[index] << endl;
	  

	  }



	} // loop over file



    }



  cout << count << endl;

 



}




int linear_index_4D(int k1, int k2, int k3, int k4) {

  // return (k1 * d2 * d3 * d4) + (k2 * d3 * d4) + (k3 * d4) + k4;

  if(k1 < n_pixel && k2 < nphotons && k3 < n_tracks && k4 < n_hyp){

  return  k1*nphotons*n_tracks*n_hyp+k2*n_tracks*n_hyp+k3*n_hyp+k4;


  }

  
  else{ return -1;}




}



int linear_index_2D(int k1, int k2) {

  // return (k1 * d2 * d3) + (k2 * d3) + k3;                                                                                

  if(k1 < n_pixel && k2 < nphotons){

    return  k1*nphotons+k2;


  }


  else{ return -2;}




}




int linear_index_3D(int k1, int k2, int k3) {

  // return (k1 * d2 * d3) + (k2 * d3) + k3;                                                                                                      

  if(k1 < n_pixel && k2 < nphotons && k3 < n_hyp){

    return  k1*nphotons*n_hyp+k2*n_hyp+k3;


  }


  else{ return -3;}




}






__device__ int Get_Track_Id(int k1, int k2) {

  // return (k1 * d2 * d3) + (k2 * d3) + k3;                                                                                                      

  if(k1 < d_n_pixel && k2 < d_nphotons){

    return  k1*d_nphotons+k2;


  }


  else{ return -2;}




}











__device__ int GetTrackCurrPID(int track_index, int *track_type_vec)
{
  return track_type_vec[track_index];
}







__global__ void matrix_sum(float* d_pixel, int* d_dims, float* d_result, int *d_hyp, int *d_tk, int *d_ph, float *d_tk_sig) {
    int tid = threadIdx.x;
    int dim_x = d_dims[0]; // hit_pixels
    int dim_y = d_dims[1]; // photons
    int dim_z = d_dims[2]; //PID_type
    int dim_m = d_dims[3]; //ntracks
 


    //int total_size = dim_x * dim_y * dim_z;  //photons,tracks,hit_pixels

    float thread_sum = 0;

    float thread_sum_tk=0;

    float global_sum=0;

    int index;

    int tk_id;
    
    int ph_id;

    int curr_pid;


    //pixel part


    for (int i = tid; i < dim_x; i += blockDim.x) {

      for(int j=0;j<dim_y;j++){
	

	tk_id=Get_Track_Id(i,j);

	
	if(tk_id>0){

	  if(d_tk[tk_id]>=0){

	
	  curr_pid=GetTrackCurrPID(d_tk[tk_id],d_hyp);  


	index= i*dim_y*dim_z+j*dim_z+curr_pid;


	
	if(index<dim_x*dim_y*dim_z){

	thread_sum += d_pixel[index];

	}


	}




	} // check if track index > 0
	



      } // loop over y axis (photons)



      //calculate here the function

      if(thread_sum>=0.001){

	thread_sum=(double)log(exp((double)thread_sum)-1);


      }


      else{

	thread_sum=(double)log(exp((double)0.001)-1);


      }




      global_sum=global_sum+thread_sum;

      thread_sum=0;
     


    } // loop over x axis (hit_pixels)




    __shared__ float s_partial_sums[256];


    s_partial_sums[tid] = global_sum;
    __syncthreads();

    for (int i = 128; i > 0; i /= 2) {
        if (tid < i) {
            s_partial_sums[tid] += s_partial_sums[tid + i];
        }
        __syncthreads();
    }

    if (tid == 0) {
        d_result[0] = s_partial_sums[0];
    }








    //track part
    
    __syncthreads();



    for (int i = tid; i < dim_m; i += blockDim.x) {

      index=i*dim_z+1;

      if(index<dim_m*dim_z){

        thread_sum_tk += d_tk_sig[index];

      }


    } // loop over tracks


    __shared__ float s_partial_sums_tk[256];


    s_partial_sums_tk[tid] = thread_sum_tk;
    __syncthreads();



    for (int i = 128; i > 0; i /= 2) {
      if (tid < i) {
	s_partial_sums_tk[tid] += s_partial_sums_tk[tid + i];
      }
      __syncthreads();
    }

    if (tid == 0) {
      d_result[0] = s_partial_sums_tk[0]-d_result[0];
    }




    // --------------------------------------------------------------------                                                                         
    // Compute complete likelihood for event with starting hypotheses                                                                               
    // --------------------------------------------------------------------  











}








void matrix_sum_wrapper(float* h_pixel, int* h_dims, float* h_result, int dimensions, int *h_hyp, int* h_tk, int * h_ph, float *h_track_sig) {


  cudaEvent_t start, stop;
  cudaEventCreate(&start);
  cudaEventCreate(&stop);




    int size_x = h_dims[0];  // hit pixels
    int size_y = h_dims[1];  // photons
    int size_z = h_dims[2]; // PID types
    int size_m = h_dims[3]; // ntracks


    int total_size = size_x * size_y*size_z;

    float* d_pixel;
    int* d_dims;
    float* d_result;
    int *d_hyp;
    int * d_tk;
    int *d_ph;
    float *d_track_sig;


    cudaMalloc((void**)&d_pixel, total_size * sizeof(float)); // pixel container
    cudaMalloc((void**)&d_dims, dimensions * sizeof(int));
    cudaMalloc((void**)&d_result, sizeof(float));
    cudaMalloc((void**)&d_hyp, n_tracks*sizeof(int)); // vector of PID hyp
    cudaMalloc((void**)&d_tk, size_x*size_y*sizeof(int)); // track container
    cudaMalloc((void**)&d_ph, size_x*size_y*sizeof(int)); // photons container
    cudaMalloc((void**)&d_track_sig, size_z*size_m*sizeof(float)); // track signal container  


    cudaMemcpy(d_pixel, h_pixel, total_size * sizeof(float), cudaMemcpyHostToDevice);
    cudaMemcpy(d_dims, h_dims, dimensions * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_hyp, h_hyp, n_tracks * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_tk, h_tk, size_x*size_y * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_ph, h_ph, size_x*size_y * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(d_track_sig, h_track_sig, size_z*size_m * sizeof(float), cudaMemcpyHostToDevice);


    cudaEventRecord(start);

    matrix_sum<<<1, 256>>>(d_pixel, d_dims, d_result, d_hyp, d_tk, d_ph, d_track_sig);

    cudaEventRecord(stop);

    cudaEventSynchronize(stop);


    float milliseconds = 0;
    cudaEventElapsedTime(&milliseconds, start, stop);

    cout << "ms " << milliseconds << endl;


    cudaMemcpy(h_result, d_result, sizeof(float), cudaMemcpyDeviceToHost);

    cudaFree(d_pixel);
    cudaFree(d_dims);
    cudaFree(d_result);
    cudaFree(d_hyp);
    cudaFree(d_tk);
    cudaFree(d_ph);
    cudaFree(d_track_sig);

}

int main() {




  /*
  int n_pixel=13000; // max number of hit pixels

  int n_hyp=4; // track hyp

  int n_tracks=128;// ntracks

  int nphotons=5; //max number of photons associated to 1 pixel  */  




  
  int h_dims[4] = {n_pixel,nphotons,n_hyp,n_tracks};
  

  //int size_evt= nphotons*n_tracks*n_pixel*n_hyp;



  int index=0;



  float *pixel_container=new float[n_pixel*n_hyp*nphotons]; 

  int *tk_container=new int[n_pixel*nphotons];

  int *ph_container=new int[n_pixel*nphotons];

  int *initial_pid=new int[n_tracks];
  
  float *track_sig_container=new float[n_tracks*n_hyp];


  // initialize container


  int PIXEL=140;

  int PHOTON_ID=0;

  int PID=0;


  FillArrayWithMCDataPixel(pixel_container,tk_container,ph_container,initial_pid);

  FillArrayWithMCDataTrack(track_sig_container);

  cout << "tk sig " << track_sig_container[0] << endl;

  /*
  if(linear_index_3D(PIXEL,PHOTON_ID,PID)!=-3){


    cout << " pixel signal is " << pixel_container[linear_index_3D(PIXEL,PHOTON_ID,PID)] <<endl;

    cout << "photon id is " << ph_container[linear_index_2D(PIXEL,PHOTON_ID)] <<endl;

    cout << "track id is " << tk_container[linear_index_2D(PIXEL,PHOTON_ID)] <<endl;
    

    

  }


  else{cout << "index out of range" << endl;}*/


  // cout << h_container[0]<<endl;
  
  //pixel, photon, track, pid

  //  cout << linear_index_4D(141,0,50,0) << endl;


  //cout << h_container[linear_index_4D(142,0,93,1)] <<endl;


  float h_result[1];

   

  matrix_sum_wrapper(pixel_container, h_dims, h_result, 4,initial_pid, tk_container, ph_container,track_sig_container);

    printf("The sum of the matrix is %f\n", h_result[0]);

  

    return 0;

  


}
