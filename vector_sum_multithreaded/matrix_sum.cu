//-*- c++ -*-                                                                                                                                    \
                                                                                                                                                  
#include <iostream>
#include <cuda.h>
#include <cuda_runtime.h>

#define BLOCK_SIZE 256

using namespace std;

__device__ bool checkSum(int sum)
{
  return sum >= 1000000;
}

__global__ void sumMatrixKernel(float *matrix, int size, int *partialSums, int *totalSum)
{
  __shared__ int s_partialSums[BLOCK_SIZE];
  int tid = threadIdx.x;
  int i = blockIdx.x * blockDim.x + threadIdx.x;
  int sum = 0;
  //while (!checkSum(*totalSum))
    //{
      s_partialSums[tid] = 0;
      for (int j = 0; j < size; j++)
        {
	  int index = i + j * size;
	  s_partialSums[tid] += (index < size * size*2) ? matrix[index] : 0;
        }
      __syncthreads();
      for (int s = blockDim.x / 2; s > 0; s >>= 1)
        {
	  if (tid < s)
            {
	      s_partialSums[tid] += s_partialSums[tid + s];
            }
	  __syncthreads();
        }
      if (tid == 0)
        {
	  atomicAdd(totalSum, s_partialSums[0]);
	  sum = s_partialSums[0];
        }
      //}
  partialSums[tid] = sum;
}

int sumMatrix(float *matrix, int size)
{
  float *d_matrix;
  int *d_partialSums, *d_totalSum, *h_partialSums, *h_totalSum, numBlocks = 1;
  size_t bytes = size * size * sizeof(float);
  cudaMalloc(&d_matrix, bytes);
  cudaMemcpy(d_matrix, matrix, bytes, cudaMemcpyHostToDevice);
  cudaMalloc(&d_partialSums, BLOCK_SIZE * sizeof(int));
  cudaMalloc(&d_totalSum, sizeof(int));
  cudaMemcpy(d_totalSum, &h_totalSum, sizeof(int), cudaMemcpyHostToDevice);

  cout << numBlocks <<endl;
  
  sumMatrixKernel<<<numBlocks, BLOCK_SIZE>>>(d_matrix, size, d_partialSums, d_totalSum);

  h_partialSums = new int[BLOCK_SIZE];
  cudaMemcpy(h_partialSums, d_partialSums, BLOCK_SIZE * sizeof(int), cudaMemcpyDeviceToHost);

  h_totalSum=new int[1];

  cudaMemcpy(h_totalSum, d_totalSum, 1 * sizeof(int), cudaMemcpyDeviceToHost);

  cout << *h_totalSum << "total sum is" << endl;



  int sum = 0;
  for (int i = 0; i < BLOCK_SIZE; i++)
    {
      sum += h_partialSums[i];
    }
  cudaFree(d_matrix);
  cudaFree(d_partialSums);
  cudaFree(d_totalSum);
  delete[] h_partialSums;
  return sum;
}

int main()
{
  int size = 256;
  float *matrix = new float[size * size];
  for (int i = 0; i < size * size; i++)
    {
      matrix[i] = 2.0;
    }
  int sum = sumMatrix(matrix, size);
  cout << "Sum of matrix is: " << sum << endl;
  delete[] matrix;
  return 0;
}
